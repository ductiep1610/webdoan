@extends('backend.master')
@section('title', 'Quản lý thuộc tính Color')
@section('content')
    <div class="row m-t-30">
        <div class="col-lg-12">
            <h3 class="title-5"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <button type="button" data-toggle="modal" data-target="#add-modal-color"
                        class="au-btn au-btn-icon btn-info au-btn--small"><i class="zmdi zmdi-plus"></i>Thêm
                        mới</button>
                </div>
            </div>
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th style="padding-bottom: 32px;">
                                <label class="au-checkbox">
                                    <input type="checkbox">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>STT</th>
                            <th>Tên</th>
                            <th>Mã</th>
                            <th>Màu</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($colors as $key => $color)
                            <tr class="color-item-{{ $color->id }}">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox">
                                        <span class="au-checkmark"></span>
                                    </label>
                                </td>
                                <td>{{ $key + 1 }}</td>
                                <td class="color-name">{{ $color->name }}</td>
                                <td class="code-color">{{ $color->code_color }}</td>
                                <td class="color" style="color: {{ $color->code_color }}"><i class="fas fa-square"
                                        style="font-size: 20px;"></i></td>
                                <td>
                                    <div class="table-data-feature" style="justify-content: flex-end;">
                                        <a href="" class="editItem" data-toggle="modal"
                                            data-target="#edit-modal-color">
                                            <button data-url="{{ route('colors.edit', ['id' => $color->id]) }}"
                                                class="item btn-edit" title="Chỉnh sửa"><i
                                                    class="zmdi zmdi-edit"></i></button>
                                        </a>
                                        <a href="" class="deleteItem"
                                            data-url="{{ route('colors.destroy', ['id' => $color->id]) }}">
                                            <button class="item btn-delete" title="Xoá"><i
                                                    class="zmdi zmdi-delete"></i></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $colors->render('backend.layouts.pagination') }}
            </div>
        </div>
    </div>
    <!-- Modal Add New Color -->
    @include('backend.colors.add-modal')
    <!-- end Modal Add New Color -->

    <!-- Modal Edit Color -->
    @include('backend.colors.edit-modal')
    <!-- end Modal Edit Color -->
@endsection
@section('script')
    {{-- Bắt lỗi modal khi submit --}}
    @if (count($errors) > 0)
        <script>
            $(document).ready(function() {
                $('#add-modal-color').modal('show');
            });
        </script>
    @endif

    <!-- Show Edit Color Ajax-->
    <script>
        $(document).on('click', '.btn-edit', function(e) {
            e.preventDefault();
            let url = $(this).attr('data-url');
            //Show Modal
            $('#edit-modal-color').modal('show');

            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    $('#name-edit').val(response.data.name);
                    $('#code_color-edit').val(response.data.code_color);
                    $('#edit_color_id').val(response.data.id);
                },
            })
        });
    </script>
    <!-- Update Color Ajax-->
    <script>
        $('#edit-form').submit(function(e) {
            e.preventDefault();

            let id = $('#edit_color_id').val();
            let url = "{{ route('colors.update') }}";
            let name_edit = $('#name-edit').val();
            let code_color_edit = $('#code_color-edit').val();
            let haserrors = $('.errors');
            let data = {
                id: id,
                name: name_edit,
                code_color: code_color_edit,
                _token: "{{ csrf_token() }}",
            };

            $.ajax({
                type: $(this).attr('method'),
                url: url,
                data: data,
                dataType: "json",
                success: function(data) {
                    $('#edit-modal-color').modal('hide');
                    $(`.color-item-${id} .color-name`).text(name_edit);
                    $(`.color-item-${id} .code-color`).text(code_color_edit);
                    $(`.color-item-${id} .color`).css('color', code_color_edit);
                    alertify.success(data.message);
                },
                error: function(data) {
                    $.each(data.responseJSON, function(key, item) {
                        haserrors.append(
                            "<small class='form-text text-muted alert-danger messages-alert'>" +
                            item + "</small>");
                    });
                },
            });
        });
    </script>
@endsection
