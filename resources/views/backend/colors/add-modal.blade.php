<div class="modal fade" id="add-modal-color" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content w-640">
            <div class="modal-header">
                <h5 class="modal-title" id="smallmodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        <form id="add-form" action="{{ route('colors.store') }}" method="post">
                            @csrf
                            <div class="card-header">Thêm mới màu sắc</div>
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="Tên màu" class=" form-control-label">Tên màu</label>
                                    <input type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Tên màu sắc" class="form-control" required>
                                    @error('name')
                                        <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="Mã màu" class=" form-control-label">Mã màu <small style="color: red;font-size:16px;">(Nhập mã màu kiểu HEX hoặc mã màu bằng tên tiếng anh)</small></label>
                                    <input type="text" id="code_color" name="code_color" value="{{ old('code_color') }}" placeholder="Ví dụ #000000 hoặc Black" class="form-control" required>
                                    @error('code_color')
                                        <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
