@extends('backend.master')
@section('title', 'Quản lý Size')
@section('content')
    <div class="row m-t-30">
        <div class="col-lg-12">
            <h3 class="title-5"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <button type="button" data-toggle="modal" data-target="#add-modal-size"
                        class="au-btn au-btn-icon btn-info au-btn--small"><i class="zmdi zmdi-plus"></i>Thêm
                        mới</button>
                </div>
            </div>
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th style="padding-bottom: 32px;">
                                <label class="au-checkbox">
                                    <input type="checkbox">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>STT</th>
                            <th>Size</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sizes as $key => $size)
                            <tr class="size-item-{{ $size->id }}">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox">
                                        <span class="au-checkmark"></span>
                                    </label>
                                </td>
                                <td>{{ $key + 1 }}</td>
                                <td class="size-name">{{ $size->size }}</td>
                                <td>
                                    <div class="table-data-feature" style="justify-content: flex-end;">
                                        <a href="" class="editItem" data-toggle="modal" data-target="#edit-modal-size">
                                            <button data-url="{{ route('sizes.edit', ['id' => $size->id]) }}" class="item btn-edit" title="Chỉnh sửa"><i
                                                    class="zmdi zmdi-edit"></i></button>
                                        </a>
                                        <a href="" class="deleteItem" data-url="{{ route('sizes.destroy', ['id' => $size->id]) }}">
                                            <button class="item btn-delete" title="Xoá"><i
                                                    class="zmdi zmdi-delete"></i></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal Add New Size -->
    @include('backend.sizes.add-modal')
    <!-- end Modal Add New Size -->

    <!-- Modal Edit Size -->
    @include('backend.sizes.edit-modal')
    <!-- end Modal Edit Size -->
@endsection
@section('script')
    <!-- Add Size Ajax-->
    <script>
        $(document).ready(function() {
            $('#add-form').submit(function(e) {
                e.preventDefault();
                let url = $(this).attr('data-url');
                let size = $('#size').val();
                let haserrors = $('.errors');

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        size: size,
                    },
                    success: function(data) {
                        $('#add-modal-size').modal('hide');
                        setTimeout(() => {
                            alertify.success(data.message);
                            location.reload();
                        }, 300);
                    },
                    error: function(data) {
                        $.each(data.responseJSON, function(key, item) {
                            haserrors.append(
                                "<small class='form-text text-muted alert-danger messages-alert'>" +item+ "</small>");
                        });
                    }
                });
            })
        });
    </script>
    <!-- Show Edit Size Ajax-->
    <script>
        $(document).on('click', '.btn-edit', function(e) {
            e.preventDefault();
            let url = $(this).attr('data-url');
            //Show Modal
            $('#edit-modal-size').modal('show');

            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    $('#size-edit').val(response.data.size);
                    $('#edit_size_id').val(response.data.id);
                },
            })
        });
    </script>
    <!-- Update Size Ajax-->
    <script>
        $('#edit-form').submit(function(e) {
            e.preventDefault();
            let id = $('#edit_size_id').val();
            let size = $('#size-edit').val();
            let haserrors = $('.errors');
            let data = {
                id: id,
                size: size,
                _token: "{{ csrf_token() }}",
            };
            // console.log(data);
            $.ajax({
                type: "post",
                url: "{{ route('sizes.update') }}",
                data: data,
                success: function(data) {
                    $('#edit-modal-size').modal('hide');
                    $(`.size-item-${id} .size-name`).text(size);
                    alertify.success(data.message);
                },
                error: function(data) {
                    $.each(data.responseJSON, function(key, item) {
                        haserrors.append(
                            "<small class='form-text text-muted alert-danger messages-alert'>" +item+ "</small>");
                    });
                },
            });
        });
    </script>
@endsection
