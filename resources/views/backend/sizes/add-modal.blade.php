<div class="modal fade" id="add-modal-size" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content w-640">
            <div class="modal-header">
                <h5 class="modal-title" id="smallmodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        <form action="" data-url="{{ route('sizes.store') }}" id="add-form" method="post">
                        @csrf
                            <div class="card-header">Thêm mới kích thước</div>
                            <div class="card-body card-block">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="text-input" class=" form-control-label">Size</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="size" name="size" placeholder="Nhập size"
                                            class="form-control" value="{{ old('size') }}" required>
                                        <div class="errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
