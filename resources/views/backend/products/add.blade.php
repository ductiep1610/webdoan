@extends('backend.master')
@section('title', 'Quản lý sản phẩm')
@section('title_form', 'Thêm mới sản phẩm')
@section('content')
    <div class="row m-t-35">
        <div class="col-lg-12">
            <div class="card">
                <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data"
                    class="form-horizontal">
                    @csrf
                    <div class="card-header">
                        <strong>@yield('title_form')</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="form-group">
                            <label for="name" class="form-control-label">Tên sản phẩm <strong class="note">*</strong></label>
                            <input type="text" name="name" value="{{ old('name') }}" placeholder="Nhập tên sản phẩm" class="form-control" required>
                            @error('name')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="select" class=" form-control-label">Thuộc danh mục <strong class="note">*</strong></label>
                            <select name="slCate" id="slCate" class="form-control">
                                <option value="" selected>Chọn Danh Mục</option>
                                <?php MenuMulti($cates, 0, $str = '---| ', old('slCate')); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price" class="form-control-label">Giá <strong class="note">*</strong></label>
                            <input type="text" name="price" placeholder="Nhập giá" class="form-control" value="{{ old('price') }}" required>
                            @error('price')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="promotion" class="form-control-label">Khuyến mãi <strong class="note">*
                                    (%)</strong></label>
                            <input type="text" name="promotion" placeholder="Nhập khuyến mãi %" required value="{{ old('promotion') }}" class="form-control">
                            @error('promotion')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description" class="form-control-label">Mô tả sản phẩm <strong class="note">*</strong></label>
                            <textarea name="description" id="description" rows="9" placeholder="Mô tả..." class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image" class=" form-control-label">Ảnh sản phẩm <strong class="note">*</strong></label>
                            <input type="file" name="image" class="form-control-file" value="{{ old('image') }}">
                            @error('image')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success btn-sm btn-add-image">Thêm ảnh chi tiết</button>
                        </div>
                        <div class="form-group add-image"></div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Lưu
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Làm mới
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#slCate").select2({
            // 'placeholder':'Chọn danh mục'
        });

        /**
         * Add Image Detail Input
         */
        $(".btn-add-image").on("click", function () {
            $(".add-image").append(`<input type="file" class="form-control-file" name="image_detail[]"></br>`);
        });

        var options = {
            filebrowserBrowseUrl: '{{ asset('backend/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('backend/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('backend/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('backend/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('backend/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('backend/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
        };
        CKEDITOR.replace('description', options);
    </script>
@endsection
