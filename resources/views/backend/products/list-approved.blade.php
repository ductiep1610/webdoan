@extends('backend.master')
@section('title', 'Danh sách sản phẩm')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <a href="{{ route('products.create') }}"><button class="au-btn au-btn-icon btn-info au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Thêm mới</button></a>

                    {{-- <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Xuất file</option>
                        <option value="">Excel</option>
                        <option value="">PDF</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div> --}}
                </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2 custom-table-data2">
                    <thead>
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>STT</th>
                            <th>Sản phẩm</th>
                            <th>Giá (VND)</th>
                            <th>Khuyến mãi (%)</th>
                            <th>Trạng thái</th>
                            <th></th>
                            <th style="text-align: center;">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($products->count() != 0)
                            @foreach ($products as $key => $product)
                                <tr class="tr-shadow">
                                    <td>
                                        <label class="au-checkbox panel">
                                            <input type="checkbox">
                                            <span class="au-checkmark checkbox_childrent"></span>
                                        </label>
                                    </td>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $product->product_name }}</td>
                                    <td class="desc">{{ $product->format_price }}</td>
                                    <td class="desc">{{ $product->promotion }}</td>
                                    <td id="txtstatus{{ $product->id }}">
                                        @if ($product->status == 1)
                                            <span class="status--process">Kích hoạt</span>
                                        @elseif($product->status == 0)
                                            <span class="status--denied">Không kích hoạt</span>
                                        @endif
                                    </td>
                                    <td>
                                        <label class="switch switch-text switch-success" style="top: 4px;">
                                            <input type="checkbox" class="switch-input change-status"
                                                @if ($product->status == 1) ? checked="true" : checked="false" @endif
                                                data-id="{{ $product->id }}"
                                                data-url="{{ route('products.change.status', ['id' => $product->id]) }}">
                                            <span data-on="On" data-off="Off" class="switch-label unactive"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="table-data-feature">
                                            
                                            <a href="{{ route('products.show', ['id' => $product->id]) }}"
                                                target="_blank"
                                                class="btn btn-hover-shine btn-outline-info border-0 btn-sm">
                                                Details
                                            </a>
                                            <a href="{{ route('products.edit', ['id' => $product->id]) }}" data-toggle="tooltip"
                                                title="Chỉnh sửa" data-placement="bottom"
                                                class="btn btn-outline-warning border-0 btn-sm">
                                                <span class="btn-icon-wrapper opacity-8">
                                                    <i class="fa fa-edit fa-w-20"></i>
                                                </span>
                                            </a>
                                            <a href="{{ route('variants.index', ['product_id' => $product->id]) }}" data-toggle="tooltip"
                                                title="Thêm thuộc tính" data-placement="bottom"
                                                class="btn btn-outline-success border-0 btn-sm">
                                                <span class="btn-icon-wrapper opacity-8">
                                                    <i class="fa fa-plus fa-w-20"></i>
                                                </span>
                                            </a>
                                            <a href="" class="deleteItem" data-url="{{ route('products.getDeleteApproved', ['id' => $product->id]) }}">
                                                <button class="btn btn-hover-shine btn-outline-danger border-0 btn-sm btn-delete" data-toggle="tooltip" title="Xoá"
                                                data-placement="bottom">
                                                    <span class="btn-icon-wrapper opacity-8"><i class="fa fa-trash fa-w-20"></i></span>
                                                </button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="tr-shadow">
                                <td colspan="8">Không có sản phẩm nào được duyệt.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
