@extends('backend.master')
@section('title', 'Danh sách chờ duyệt')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <a href="{{ route('products.create') }}">
                        <button class="au-btn au-btn-icon btn-info au-btn--small"><i class="zmdi zmdi-plus"></i>Thêm
                            mới</button>
                    </a>
                </div>
            </div>
            <div class="user-data">
                <div class="table-responsive table-data custom-table-data">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>STT</td>
                                <td>Sản phẩm</td>
                                <td>Giá (VND)</td>
                                <td>Khuyến mãi (%)</td>
                                <td>Trạng thái</td>
                                <td style="text-align: center;padding-right:40px;">Thao tác</td>
                                @if (auth()->guard('admin')->user()->level == 2)
                                    <td></td>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if ($products->count() != 0)
                                @foreach ($products as $key => $product)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                            <div class="table-data__info">
                                                <h6>{{ $product->product_name }}</h6>
                                                <span>
                                                    <small style="color: brown;">{{ $product->category->name }}</small>
                                                </span>
                                            </div>
                                        </td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->promotion }}</td>
                                        <td>
                                            @if ($product->status == 2)
                                                <span class="role admin">Chờ duyệt</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="table-data-feature">
                                                <a href="{{ route('products.show', ['id' => $product->id]) }}"
                                                    target="_blank"
                                                    class="btn btn-hover-shine btn-outline-info border-0 btn-sm">
                                                    Details
                                                </a>
                                                <a href="{{ route('products.edit', ['id' => $product->id]) }}" data-toggle="tooltip"
                                                    title="Chỉnh sửa" data-placement="bottom"
                                                    class="btn btn-outline-warning border-0 btn-sm">
                                                    <span class="btn-icon-wrapper opacity-8">
                                                        <i class="fa fa-edit fa-w-20"></i>
                                                    </span>
                                                </a>
                                                <a href="" class="deleteItem" data-url="{{ route('products.destroy', ['id' => $product->id]) }}">
                                                    <button class="btn btn-hover-shine btn-outline-danger border-0 btn-sm btn-delete" data-toggle="tooltip" title="Xoá"
                                                    data-placement="bottom">
                                                        <span class="btn-icon-wrapper opacity-8"><i class="fa fa-trash fa-w-20"></i></span>
                                                    </button>
                                                </a>
                                                <a href="{{ route('variants.index', ['product_id' => $product->id]) }}" data-toggle="tooltip"
                                                    title="Thêm thuộc tính" data-placement="bottom"
                                                    class="btn btn-outline-success border-0 btn-sm">
                                                    <span class="btn-icon-wrapper opacity-8">
                                                        <i class="fa fa-plus fa-w-20"></i>
                                                    </span>
                                                </a>
                                            </div>
                                        </td>
                                        @if (auth()->guard('admin')->user()->level == 2)
                                            <td>
                                                <a href="" class="approved-product"
                                                    data-url="{{ route('products.getbrowseProduct', ['id' => $product->id]) }}">
                                                    <input class="btn btn-outline-primary" type="submit" value="Duyệt">
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">Không có sản phẩm nào trong danh sách.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.approved-product').click(function(e) {
            e.preventDefault();
            let url = $(this).data('url');
            let that = $(this);

            if (confirm('Bạn muốn duyệt sản phẩm này?')) {
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function(data) {
                        alertify.success(data.message);
                        that.parent().parent().parent().remove();
                    },
                    error: function(data) {

                    }
                });
            }
        })
    </script>
@endsection
