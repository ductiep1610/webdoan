@extends('backend.master')
@section('title', 'Quản lý sản phẩm')
@section('title_form', 'Chỉnh sửa sản phẩm')
@section('content')
    <div class="row m-t-35">
        <div class="col-lg-12">
            <div class="card">
                <form action="{{ route('products.update', ['id' => $product->id]) }}" method="post"
                    enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    <div class="card-header">
                        <strong>@yield('title_form')</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="form-group">
                            <label for="name" class="form-control-label">Tên sản phẩm <strong class="note">*</strong></label>
                            <input type="text" name="name" value="{{ $product['product_name'] }}"
                                placeholder="Nhập tên sản phẩm" class="form-control" required>
                            @error('name')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="select" class=" form-control-label">Thuộc danh mục <strong class="note">*</strong></label>
                            <select name="slCate" id="slCate" class="form-control">
                                <option value="" selected>Chọn Danh Mục</option>
                                <?php MenuMulti($cates, 0, $str = '---| ', $product['cate_id']); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price" class="form-control-label">Giá <strong class="note">*</strong></label>
                            <input type="text" name="price" placeholder="Nhập giá" class="form-control"
                                value="{{ $product['price'] }}" required>
                            @error('price')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="promotion" class="form-control-label">Khuyến mãi <strong class="note">* (%)</strong>
                            </label>
                            <input type="text" name="promotion" placeholder="Nhập khuyến mãi %" required
                                value="{{ $product['promotion'] }}" class="form-control">
                            @error('promotion')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description" class="form-control-label">Mô tả sản phẩm <strong class="note">*</strong></label>
                            <textarea name="description" id="description" rows="9" placeholder="Mô tả..." class="form-control">{{ $product['description'] }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="image" class=" form-control-label">Ảnh sản phẩm <strong class="note">*</strong></label>
                            <input type="file" name="image" class="form-control-file">
                            <img class="img-fluid" src="{{ asset('uploads/products/' . $product['image']) }}"
                                alt="{{ $product['product_name'] }}">
                            @error('image')
                                <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="image" class=" form-control-label">Ảnh chi tiết <strong class="note">*</strong></label><br>
                            <div class="row form-group">
                                @foreach ($imagesDetail as $key => $imagedetail)
                                <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top" src="{{ asset('uploads/imagesdetail/' . $imagedetail['image_detail']) }}" alt="{{ $product['product_name'] }}">
                                        <button type="button" data-url="{{ route('products.getdelImageDetail', ['id' => $product->id, 'id_image' => $imagedetail->id]) }}" class="close note deleteImage"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                </div> 
                                @endforeach
                            </div> 
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success btn-sm btn-add-image">Thêm ảnh chi tiết</button>
                        </div>
                        <div class="form-group add-image"></div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Lưu
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Làm mới
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#slCate").select2({
            // 'placeholder':'Chọn danh mục'
        });

        /**
         * Add Image Detail Input
         */
        $(".btn-add-image").on("click", function() {
            $(".add-image").append(`<input type="file" class="form-control-file" name="image_detail[]"></br>`);
        });

        /**
         * Delete Image Detail
         */
        function deleteImageDetail(event) {
            event.preventDefault();
            var url = $(this).data('url');
            var that = $(this);
            if (confirm('Bạn muốn xoá ảnh này ?')) {
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function(data) {
                        that.parent().parent().remove();
                        alertify.success(data.message);
                    },
                    error: function() {

                    }
                })
            }
        }
        $(function() {
            $(document).on('click', '.deleteImage', deleteImageDetail);
        })
            
        var options = {
            filebrowserBrowseUrl: '{{ asset('backend/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('backend/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('backend/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('backend/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('backend/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('backend/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
        };
        CKEDITOR.replace('description', options);
    </script>
@endsection
