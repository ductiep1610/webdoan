@extends('backend.master')
@section('title', 'Chi tiết sản phẩm' . ' ' . $product->product_name)
@section('content')
    <div class="row m-t-30">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body display_data">
                    <div class="position-relative row form-group">
                        <label for="" class="col-md-3 text-md-right col-form-label">Ảnh đại diện</label>
                        <div class="col-md-9 col-xl-8">
                            <ul class="text-nowrap overflow-auto" id="images">
                                <li class="d-inline-block mr-1" style="position: relative;">
                                    <img style="height: 150px;" src="{{ $product->image_url }}" alt="Image">
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="" class="col-md-3 text-md-right col-form-label">Ảnh chi tiết sản
                            phẩm</label>
                        <div class="col-md-9 col-xl-8">
                            <ul class="text-nowrap overflow-auto" id="images">
                                @foreach ($product->images as $productImage)
                                    <li class="d-inline-block mr-1" style="position: relative;">
                                        <img style="height: 150px;"
                                            src="{{ asset('uploads/imagesdetail/' . $productImage->image_detail) }}"
                                            alt="Image">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="name" class="col-md-3 text-md-right col-form-label">Sản phẩm</label>
                        <div class="col-md-9 col-xl-8">
                            <p>{{ $product->product_name }}</p>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="product_category_id" class="col-md-3 text-md-right col-form-label">Danh mục</label>
                        <div class="col-md-9 col-xl-8">
                            <p>{{ $product->category->name }}</p>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="name" class="col-md-3 text-md-right col-form-label">Người đăng</label>
                        <div class="col-md-9 col-xl-8">
                            <p>{{ $product->admin->name }}</p>
                        </div>
                    </div>

                    {{-- <div class="position-relative row form-group">
                        <label for="content" class="col-md-3 text-md-right col-form-label">Content</label>
                        <div class="col-md-9 col-xl-8">
                            <p>High quality fabric, modern and youthful design</p>
                        </div>
                    </div> --}}

                    <div class="position-relative row form-group">
                        <label for="price" class="col-md-3 text-md-right col-form-label">Giá</label>
                        <div class="col-md-9 col-xl-8">
                            <p>{{ $product->format_price }} VNĐ</p>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="discount" class="col-md-3 text-md-right col-form-label">Giá khuyến mãi</label>
                        <div class="col-md-9 col-xl-8">
                            <p>{{ $product->format_promotion }} VNĐ</p>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="qty" class="col-md-3 text-md-right col-form-label">Số lượng</label>
                        <div class="col-md-9 col-xl-8">
                            <p>{{ $product->qty }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
