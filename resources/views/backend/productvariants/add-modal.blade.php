<div class="modal fade" id="add-modal-variant" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content w-640">
            <div class="modal-header">
                <h5 class="modal-title" id="smallmodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        <form action="{{ route('variants.store') }}" id="add-form" method="post">
                        @csrf
                            <div class="card-header">Thêm mới thuộc tính</div>
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <div class="card-body card-block">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="color_id" class=" form-control-label">Sản phẩm</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text"  name="product_name" value="{{ $product->product_name }}" disabled="disabled" class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="color_id" class=" form-control-label">Màu sắc</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="color_id" id="color_id" class="form-control">
                                            <option value="">Chọn màu sắc</option>
                                            @foreach ($colors as $color)    
                                                <option style="color: {{ $color->code_color }}" value="{{ $color->id }}">{{ $color->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="size_id" class=" form-control-label">Kích thước</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="size_id" id="size_id" class="form-control">
                                            <option value="">Chọn kích thước</option>
                                            @foreach ($sizes as $size)    
                                                <option value="{{ $size->id }}">{{ $size->size }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="text-input" class=" form-control-label">Số lượng</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="qty" name="qty" placeholder="Nhập số lượng"
                                            class="form-control" value="{{ old('qty') }}" required>
                                        <div class="errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>