@extends('backend.master')
@section('title', 'Quản lý thuộc tính')
@section('content')
    <div class="row m-t-35">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="{{ route('variants.update', ['product_id' => $product->id, 'id' => $productVariant->id ]) }}" method="post">
                        @csrf
                        <div class="position-relative row form-group">
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <label class="col-md-3 text-md-right col-form-label">Sản phẩm</label>
                            <div class="col-md-9 col-xl-8">
                                <input disabled placeholder="Product Name" type="text" class="form-control"
                                    value="{{ $product->product_name }}">
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="color_id" class="col-md-3 text-md-right col-form-label">Màu sắc</label>
                            <div class="col-md-9 col-xl-8">
                                <select name="color_id" id="color_id" class="form-control">
                                    <option value="">Chọn màu sắc</option>
                                    @foreach ($colors as $color)    
                                        <option style="color: {{ $color->code_color }}" value="{{ $color->id }}"
                                            @if ($color->id == $productVariant->color_id) selected @endif    
                                        >{{ $color->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="size_id" class="col-md-3 text-md-right col-form-label">Kích thước</label>
                            <div class="col-md-9 col-xl-8">
                                <select name="size_id" id="size_id" class="form-control">
                                    <option value="">Chọn kích thước</option>
                                    @foreach ($sizes as $size)    
                                        <option value="{{ $size->id }}" 
                                            @if ($size->id == $productVariant->size_id) selected @endif
                                        >{{ $size->size }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="qty" class="col-md-3 text-md-right col-form-label">Số lượng</label>
                            <div class="col-md-9 col-xl-8">
                                <input required name="qty" id="qty" placeholder="Nhập số lượng" type="text"
                                    class="form-control" value="{{ $productVariant->qty }}">
                            </div>
                        </div>

                        <div class="position-relative row form-group mb-1">
                            <div class="col-md-9 col-xl-8 offset-md-2">
                                <a href="#" class="border-0 btn btn-outline-danger mr-1">
                                    <span class="btn-icon-wrapper pr-1 opacity-8">
                                        <i class="fa fa-times fa-w-20"></i>
                                    </span>
                                    <span>Cancel</span>
                                </a>

                                <button type="submit" class="btn-shadow btn-hover-shine btn btn-primary">
                                    <span class="btn-icon-wrapper pr-2 opacity-8">
                                        <i class="fa fa-download fa-w-20"></i>
                                    </span>
                                    <span>Lưu</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#size_id").select2({
            idth: 'resolve'
        });
    </script>
@endsection
