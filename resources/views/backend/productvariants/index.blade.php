@extends('backend.master')
@section('title', 'Danh sách thuộc tính sản phẩm')
@section('content')
    <div class="row m-t-35">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="page-title-actions">
                                <a href="" data-toggle="modal" data-target="#add-modal-variant" class="btn-shadow btn-hover-shine mr-3 btn btn-primary">
                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                        <i class="fa fa-plus fa-w-20"></i>
                                    </span>
                                    Thêm mới
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th class="pl-4">Sản phẩm</th>
                                <th>Màu sắc</th>
                                <th>kích thước</th>
                                <th>Số lượng</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($productVariants->count())
                                @foreach ($productVariants as $productVariant)
                                <tr>
                                    <td class="pl-4 text-muted">{{ $product->product_name }}</td>
                                    <td style="color: {{ $productVariant->color->code_color }}">{{ $productVariant->color->name }} - <i class="fas fa-square"
                                        style="font-size: 20px;"></i> </td>
                                    <td class="">{{ $productVariant->size->size }}</td>
                                    <td class="">{{ $productVariant->qty }}</td>

                                    <td class="text-center">
                                        <div class="table-data-feature">
                                            <a href="{{ route('variants.edit', ['product_id' => $product->id, 'id' => $productVariant->id]) }}" data-toggle="tooltip" title="Chỉnh sửa"
                                                data-placement="bottom" class="btn btn-outline-warning border-0 btn-sm">
                                                <span class="btn-icon-wrapper opacity-8">
                                                    <i class="fa fa-edit fa-w-20"></i>
                                                </span>
                                            </a>
                                            <a href="" class="deleteItem" data-url="{{ route('variants.destroy', ['product_id' => $product->id, 'id' => $productVariant->id]) }}">
                                                <button class="btn btn-hover-shine btn-outline-danger border-0 btn-sm btn-delete"
                                                type="submit" data-toggle="tooltip" title="Xoá" data-placement="bottom">
                                                    <span class="btn-icon-wrapper opacity-8">
                                                        <i class="fa fa-trash fa-w-20"></i>
                                                    </span>
                                                </button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @else 
                                <tr>
                                    <td colspan="5">Không có dữ liệu thuộc tính nào.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="d-block card-footer">

                </div>
            </div>
        </div>
    </div>
    <!-- Include Modal Add Variant  -->
    @include('backend.productvariants.add-modal')
@endsection
@section('script')
    <script>
        $("#size_id").select2({
            idth: 'resolve'
        });
    </script>
@endsection

