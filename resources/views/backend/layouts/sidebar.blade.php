<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="{{ asset('backend/images/icon/logo.png') }}" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="active has-sub">
                    <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Dashboard</a>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-group"></i>Quản lý hệ thống</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{ route('accounts.index') }}">Tài khoản</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Quản lý danh mục</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{ route('category.index') }}">Loại sản phẩm</a>
                        </li>
                        <li>
                            <a href="{{ route('coupons.index') }}">Mã giảm giá</a>
                        </li>
                        <li>
                            <a href="{{ route('sizes.index') }}">Kích thước</a>
                        </li>
                        <li>
                            <a href="{{ route('colors.index') }}">Màu sắc</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-shopping-cart"></i>Quản lý sản phẩm</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{ route('products.create') }}">Thêm mới</a>
                        </li>
                        <li>
                            <a href="{{ route('products.index') }}">Sản phẩm chờ duyệt</a>
                        </li>
                        <li>
                            <a href="{{ route('products.listApproved') }}">Sản phẩm đã duyệt</a>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </nav>
    </div>
</aside>