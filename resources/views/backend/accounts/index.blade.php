@extends('backend.master')
@section('title', 'Danh sách tài khoản')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <button type="button" data-toggle="modal" data-target="#add-modal-account"
                        class="au-btn au-btn-icon btn-info au-btn--small"><i class="zmdi zmdi-plus"></i>Thêm
                        mới</button>
                </div>
            </div>
            <div class="user-data">
                <div class="table-responsive table-data">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox">
                                        <span class="au-checkmark"></span>
                                    </label>
                                </td>
                                <td>STT</td>
                                <td>Tài khoản</td>
                                <td>Role</td>
                                <td>Trạng thái</td>
                                <td></td>
                                <td style="text-align: center;">Thao tác</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($accounts as $key => $item)
                                <tr class="account-item-{{ $item->id }}">
                                    <td>
                                        <label class="au-checkbox">
                                            <input type="checkbox">
                                            <span class="au-checkmark"></span>
                                        </label>
                                    </td>
                                    <td>{{ $key + 1 }}</td>
                                    <td>
                                        <div class="table-data__info">
                                            <h6 class="account-name">{{ $item->name }}</h6>
                                            <span>
                                                <small>{{ $item->email }}</small>
                                            </span>
                                        </div>
                                    </td>
                                    @if ($item->level == 2)
                                        <td class="update-role"><span class="role admin">Admin</span></td>
                                    @else
                                        <td class="update-role"><span class="role member">Member</span></td>
                                    @endif
                                    <td class="status" id="txtstatus{{ $item->id }}">{!! $item->statusactive !!}</td>
                                    <td>
                                        <label class="switch switch-3d switch-success mr-3">
                                            <input type="checkbox" data-id="{{ $item->id }}"
                                                data-url="{{ route('accounts.change.status', ['id' => $item->id]) }}"
                                                data-id="{{ $item->id }}" class="switch-input"
                                                @if ($item->status == 1) ?  checked="true" : checked="false" @endif>
                                            <span class="switch-label unactive"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="table-data-feature">
                                            <a class="editItem" data-toggle="modal"
                                                data-target="#edit-modal-account">
                                                <button data-url="{{ route('accounts.edit', ['id' => $item->id]) }}"
                                                    class="item btn-edit" title="Chỉnh sửa"><i
                                                        class="zmdi zmdi-edit"></i></button>
                                            </a>
                                            <a class="deleteItem"
                                                data-url="{{ route('accounts.destroy', ['id' => $item->id]) }}">
                                                <button class="item btn-delete" title="Xoá"><i
                                                        class="zmdi zmdi-delete"></i></button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <button type="button" data-toggle="modal" data-target="#add-modal-account"
                        class="au-btn au-btn-icon btn-info au-btn--small"><i class="zmdi zmdi-plus"></i>Thêm
                        mới</button>
                </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2 custom-table">
                    <thead>
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>STT</th>
                            <th>Họ & Tên</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Trạng thái</th>
                            <th></th>
                            <th style="text-align: center">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($accounts as $key => $item)
                            <tr class="tr-shadow account-item-{{ $item->id }}">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox"><span class="au-checkmark"></span>
                                    </label>
                                </td>
                                <td>{{ $key + 1 }}</td>
                                <td class="account-name">{{ $item->name }}</td>
                                <td class="desc">{{ $item->email }}</td>
                                @if ($item->level == 3)
                                    <td class="desc role">Quản trị viên</td>
                                @elseif($item->level == 2)
                                    <td class="desc role">Cộng tác viên</td>
                                @else
                                    <td class="desc role">Thành viên</td>
                                @endif
                                <td class="status" id="txtstatus{{ $item->id }}">{!! $item->statusactive !!}</td>
                                <td>
                                    <label class="switch switch-3d switch-success mr-3">
                                        <input type="checkbox" data-id="{{ $item->id }}"
                                            data-url="{{ route('accounts.change.status', ['id' => $item->id]) }}"
                                            data-id="{{ $item->id }}" class="switch-input"
                                            @if ($item->status == 1) ?  checked="true" : checked="false" @endif>
                                        <span class="switch-label unactive"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <a class="editItem" href="" data-toggle="modal" data-target="#edit-modal-account">
                                            <button data-url="{{ route('accounts.edit', ['id' => $item->id]) }}"
                                                class="item btn-edit" title="Chỉnh sửa"><i
                                                    class="zmdi zmdi-edit"></i></button>
                                        </a>
                                        <a href="" class="deleteItem"
                                            data-url="{{ route('accounts.destroy', ['id' => $item->id]) }}">
                                            <button class="item btn-delete" title="Xoá"><i
                                                    class="zmdi zmdi-delete"></i></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div> --}}
    <!-- Modal Add New Account -->
    @include('backend.accounts.add-modal')
    <!-- end Modal Add New -->

    <!-- Modal Edit Account -->
    @include('backend.accounts.edit-modal')
    <!-- end Modal Edit -->
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#add-form').submit(function(event) {
                event.preventDefault();
                let url = $(this).attr('data-url');
                // Lấy giá trị input form nhập vào
                let name = $('#name').val();
                let email = $('#email').val();
                let password = $('#password').val();
                let level = $('input[name="level"]:checked').val();
                let status = $('input[name="status"]:checked').val();

                // Call Ajax
                $.ajax({
                    type: 'post',
                    url: url,
                    data: {
                        name: name,
                        email: email,
                        password: password,
                        level: level,
                        status: status
                    },
                    beforeSend: function (){
                        $.LoadingOverlay('show');
                    },
                    success: function(data) {
                        $('#add-modal-account').modal('hide');
                        $.LoadingOverlay('hide');
                        location.reload();
                        // alertify.success(data.message);
                    },
                    error: function(xhr) {
                        $.LoadingOverlay('hide');
                    },
                })
            });
        });
    </script>
    <script>
        $(document).on('click', '.btn-edit', function(e) {
            e.preventDefault();
            // $('#edit-modal-account').modal('show');
            let url = $(this).attr('data-url');

            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function (){
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#name-edit').val(response.data.name);
                    $('#email-edit').val(response.data.email);
                    $('#password-edit').val(response.data.password);
                    $('input[name=level_edit][value="' + response.data.level + '"]').prop('checked',
                        true);
                    $('#edit_account_id').val(response.data.id);
                    // $('input[name=status_edit][value="' + response.data.status + '"]').prop('checked',true);
                },
                error: function (xhr){
                    $.LoadingOverlay('hide');
                }
            })
        });
    </script>
    <script>
        $('#edit-form').submit(function(e) {
            e.preventDefault();
            let id = $('#edit_account_id').val();
            let name = $('#name-edit').val();
            let level = $('input[name="level_edit"]:checked').val();
            // let status = $('input[name="status_edit"]:checked').val()
            // let password = $('#password_edit').val();

            let admin = '<span class="role admin">' + 'Admin' + '</span>';
            let member = '<span class="role member">' + 'Member' + '</span>';

            let data = {
                id: id,
                name: name,
                level: level,
                _token: "{{ csrf_token() }}",
            }
            $.ajax({
                type: "post",
                url: "{{ route('accounts.update') }}",
                data,
                success: function(response) {
                    $(`.account-item-${id} .account-name`).text(name);
                    if (level == 2) {
                        $(`.account-item-${id} .update-role`).html(admin);
                    } else {
                        $(`.account-item-${id} .update-role`).html(member);
                    }

                    $('#edit-modal-account').modal('hide');
                    alertify.success(response.message);
                }
            });
        });
    </script>
@endsection
