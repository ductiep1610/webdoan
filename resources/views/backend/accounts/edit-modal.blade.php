<div class="modal fade" id="edit-modal-account" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content w-640">
            <div class="modal-header">
                <h5 class="modal-title" id="smallmodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        <form action="" method="post" id="edit-form">
                            @csrf
                            <input type="hidden" id="edit_account_id">
                            <div class="card-header">Chỉnh sửa tài khoản</div>
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="text" id="name-edit" name="name_edit" placeholder="Họ & tên"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                        <input type="email" id="email-edit" name="email_edit" placeholder="Email"
                                            disabled="disabled" class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-lg-1">
                                        <label class="form-control-label">Role</label>
                                    </div>
                                    <div class="col col-lg-11">
                                        <div class="form-check-inline form-check input-check">
                                            <input type="radio" id="inline-radio1" name="level_edit" value="1"
                                                class="form-check-input" checked="checked">
                                            <label for="inline-radio2" class="form-check-label">Thành viên</label>
                                            <input type="radio" id="inline-radio2" name="level_edit" value="2"
                                                class="form-check-input">
                                            <label for="inline-radio1" class="form-check-label">Quản trị
                                                viên</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn_update">Lưu</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="form-group">
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
        <input type="password" id="password-edit" disabled="disabled" name="password_edit" placeholder="Password" class="form-control">
    </div>
</div>
<div class="row form-group">
    <div class="col col-lg-10">
        <div class="form-check">
            <div class="checkbox">
                <input type="checkbox" id="checkbox" name="checkbox1" value="option" class="form-check-input click-change-password">Đổi mật khẩu
            </div>
        </div>
    </div>
</div>
<div class="row form-group">
    <div class="col col-md-2">
        <label class="form-control-label">Trạng thái</label>
    </div>
    <div class="col col-md-10">
        <div class="form-check-inline form-check input-check">
            <input type="radio" id="inline-radio2" name="status_edit" value="1"
                class="form-check-input" checked="checked">
            <label for="inline-radio2" class="form-check-label">Kích hoạt</label>
            <input type="radio" id="inline-radio1" name="status_edit" value="0"
                class="form-check-input">
            <label for="inline-radio1" class="form-check-label">Không kích hoạt</label>
        </div>
    </div>
</div> --}}
