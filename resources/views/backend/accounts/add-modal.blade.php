<div class="modal fade" id="add-modal-account" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content  w-640">
            <div class="modal-header">
                <h5 class="modal-title" id="smallmodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        <form action="" data-url="{{ route('accounts.store') }}" id="add-form" method="post">
                            @csrf
                            <div class="card-header">Thêm mới tài khoản</div>
                            <div class="card-body card-block">
                                <div class="alert alert-danger" style="display:none"></div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" id="name" name="name" placeholder="Họ & tên"
                                            class="form-control">
                                    </div>
                                    {{-- <small class="form-text text-muted alert-danger messages-alert name_error"></small> --}}
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                        <input type="email" id="email" name="email" placeholder="Email"
                                            class="form-control">
                                    </div>
                                    {{-- <small class="form-text text-muted alert-danger messages-alert email_error"></small> --}}
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                        <input type="password" id="password" name="password" placeholder="Password"
                                            class="form-control">
                                    </div>
                                    {{-- <small class="form-text text-muted alert-danger messages-alert password_error"></small> --}}
                                </div>
                                <div class="row form-group">
                                    <div class="col col-lg-3">
                                        <label class="form-control-label">Role</label>
                                    </div>
                                    <div class="col col-lg-9">
                                        <div class="form-check-inline form-check input-check">
                                            <label for="inline-radio2" class="form-check-label">
                                                <input type="radio" id="inline-radio1" name="level" value="1"
                                                    class="form-check-input" checked="checked"> Thành viên
                                            </label>

                                            <label for="inline-radio1" class="form-check-label">
                                                <input type="radio" id="inline-radio2" name="level" value="2"
                                                    class="form-check-input"> Quản trị viên
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class="form-control-label">Trạng thái</label>
                                    </div>
                                    <div class="col col-md-9">
                                        <div class="form-check-inline form-check input-check">
                                            <input type="radio" id="inline-radio2" name="status" value="1"
                                                class="form-check-input" checked="checked">
                                            <label for="inline-radio2" class="form-check-label">Kích hoạt</label>
                                            <input type="radio" id="inline-radio1" name="status" value="0"
                                                class="form-check-input">
                                            <label for="inline-radio1" class="form-check-label">Không</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
