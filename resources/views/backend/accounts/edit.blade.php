@extends('backend.master')
@section('title', 'Chỉnh sửa tài khoản')
@section('title_form', 'Chỉnh sửa')
@section('content')
    <div class="row m-t-35">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">Chỉnh sửa tài khoản</div>
                <div class="card-body card-block">
                    <form action="{{ route('accounts.update', ['id'=> $account->id]) }}" method="post" id="edit-form">
                        @csrf
                        <input type="hidden" id="edit_account_id">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input type="text" id="name-edit" value="{{ $account->name }}" name="name_edit" placeholder="Họ & tên"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input type="email" id="email-edit" value="{{ $account->email }}" name="email_edit" placeholder="Email"
                                    disabled="disabled" class="form-control">
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                <input type="password" id="password-edit" value="{{ $account->passrord }}" disabled="disabled" name="password_edit"
                                    placeholder="Password" class="form-control">
                            </div>
                        </div> --}}
                        {{-- <div class="row form-group">
                            <div class="col col-lg-10">
                                <div class="form-check">
                                    <div class="checkbox">
                                        <input type="checkbox" id="checkbox" name="checkbox1" value="option"
                                            class="form-check-input click-change-password">Đổi mật khẩu
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row form-group">
                            <div class="col col-lg-2">
                                <label class="form-control-label">Role</label>
                            </div>
                            <div class="col col-lg-10">
                                <div class="form-check-inline form-check input-check">
                                    <input type="radio" id="inline-radio1" name="level_edit" value="1"
                                        class="form-check-input" @if ($account->level == 1) checked @endif>
                                    <label for="inline-radio2" class="form-check-label">Thành viên</label>
                                    <input type="radio" id="inline-radio2" name="level_edit" value="2"
                                        class="form-check-input" @if ($account->level == 2) checked @endif>
                                    <label for="inline-radio1" class="form-check-label">Cộng tác
                                        viên</label>
                                    <input type="radio" id="inline-radio3" name="level_edit" value="3"
                                        class="form-check-input" @if ($account->level == 3) checked @endif>
                                    <label for="inline-radio1" class="form-check-label ">Quản trị
                                        viên</label>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label class="form-control-label">Trạng thái</label>
                            </div>
                            <div class="col col-md-10">
                                <div class="form-check-inline form-check input-check">
                                    <input type="radio" id="inline-radio2" name="status_edit" value="1"
                                        class="form-check-input" @if ($account->status == 1) checked @endif>
                                    <label for="inline-radio2" class="form-check-label">Kích hoạt</label>
                                    <input type="radio" id="inline-radio1" name="status_edit" value="0"
                                        class="form-check-input" @if ($account->status == 0) checked @endif>
                                    <label for="inline-radio1" class="form-check-label">Không kích hoạt</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions form-group" style="text-align: center">
                            <button type="submit" class="btn btn-success btn_update">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.click-change-password').click(function() {
            if ($(this).is(':checked')) {
                $('#password-edit').prop("disabled", false);
            } else {
                $('#password-edit').prop("disabled", true);
            }
        });
    </script>
@endsection
