@include('backend.layouts.head')

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        @include('backend.layouts.header-mobile')
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR DESKTOP-->
        @include('backend.layouts.sidebar')
        <!-- END MENU SIDEBAR DESKTOP-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            @include('backend.layouts.header-desktop')
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        @include('messages')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">@yield('title')</h2>
                                </div>
                            </div>
                        </div>
                        @yield('content')

@include('backend.layouts.footer')

