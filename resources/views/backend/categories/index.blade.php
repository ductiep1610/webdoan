@extends('backend.master')
@section('title', 'Danh sách Category')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35"></h3>
            <div class="table-data__tool">
                {{-- <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div> --}}
                <div class="table-data__tool-right">
                    <a href="{{ route('category.create') }}"><button class="au-btn au-btn-icon btn-info au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Thêm mới</button></a>

                    <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                        <select class="js-select2" name="type">
                            <option selected="selected">Xuất file</option>
                            <option value="">Excel</option>
                            <option value="">PDF</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>STT</th>
                            <th>Tên danh mục</th>
                            <th>Slug</th>
                            <th>Trạng thái</th>
                            <th></th>
                            <th style="text-align: center;padding-right: 40px;">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cates as $key=> $item)
                            <tr class="tr-shadow">
                                <td>
                                    <label class="au-checkbox panel">
                                        <input type="checkbox">
                                        <span class="au-checkmark checkbox_childrent"></span>
                                    </label>
                                </td>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $item->name }}</td>
                                <td class="desc">{{ $item->slug }}</td>
                                <td id="txtstatus{{ $item->id }}">{!! $item->statusactive !!}</td>
                                <td>
                                    <label class="switch switch-3d switch-success mr-3">
                                        <input type="checkbox" data-id="{{ $item->id }}" data-url="{{ route('category.change.status',['id'=>$item->id]) }}" class="switch-input" @if ($item->status == 1) ?  checked="true" : checked="false" @endif>
                                            <span class="switch-label unactive"></span>
                                            <span class="switch-handle"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <a class="editItem" href="{{ route('category.edit', ['cate_slug' => $item->slug]) }}">
                                            <button class="item btn-edit" title="Chỉnh sửa"><i class="zmdi zmdi-edit"></i></button>
                                        </a>
                                        <a href="" class="deleteItem" data-url="{{ route('category.destroy', ['id' => $item->id]) }}">
                                            <button class="item btn-delete" title="Xoá"><i class="zmdi zmdi-delete"></i></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $cates->render('backend.layouts.pagination') }}
            </div>
        </div>
    </div>
@endsection
