@extends('backend.master')
@section('title', 'Quản lý loại sản phẩm')
@section('title_form', 'Thêm mới loại sản phẩm')
@section('content')
    <div class="row m-t-35">
        <div class="col-lg-10">
            <div class="card">
                <form action="{{ route('category.store') }}" method="post" enctype="multipart/form-data"
                    class="form-horizontal">
                    @csrf
                    <div class="card-header">
                        <strong>@yield('title_form')</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="text-input" class=" form-control-label">Tên danh mục</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="text-input" name="name" placeholder="Tên danh mục"
                                    class="form-control" value="{{ old('name') }}">
                                @error('name')
                                    <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="select" class=" form-control-label">Thuộc danh mục</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <select name="sltCate" id="js-example-responsive" class="form-control">
                                    <option value="0">--ROOT--</option>
                                    <?php MenuMulti($cates, 0, $str = '---| ', old('sltCate')); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class="form-control-label">Trạng thái</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check input-check">
                                    <input type="radio" id="inline-radio2" name="status" value="1"
                                        class="form-check-input" checked="checked">
                                    <label for="inline-radio2" class="form-check-label ">Kích hoạt</label>
                                    <input type="radio" id="inline-radio1" name="status" value="0"
                                        class="form-check-input">
                                    <label for="inline-radio1" class="form-check-label ">Ẩn</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Lưu
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Làm mới
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#js-example-responsive").select2({
            idth: 'resolve'
        });
    </script>
@endsection
