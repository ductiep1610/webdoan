@extends('backend.master')
@section('title', 'Quản lý mã giảm giá')
@section('title_form', 'Thêm mới mã giảm giá')
@section('content')
    <div class="row m-t-35">
        <div class="col-lg-10">
            <div class="card">
                <form action="{{ route('coupons.store') }}" method="post" class="form-horizontal">
                    @csrf
                    <div class="card-header">
                        <strong>@yield('title_form')</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="text-input" class=" form-control-label">Mã giảm giá</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="text-input" name="code" placeholder="Mã giảm giá"
                                    class="form-control" value="{{ old('code') }}">
                                @error('code')
                                    <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="text-input" class=" form-control-label">Giá trị</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="text-input" name="value" placeholder="%"
                                    class="form-control" value="{{ old('value') }}">
                                @error('value')
                                    <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="cc-exp" class="control-label mb-1">Ngày bắt đầu</label>
                                    <input id="start_date" name="start_date" type="date" class="form-control cc-exp" value="{{ old('start_date') }}">
                                </div>
                                @error('start_date')
                                    <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="cc-exp" class="control-label mb-1">Ngày kết thúc</label>
                                    <input name="end_date" type="date" class="form-control cc-exp" value="{{ old('end_date') }}">
                                </div>
                                @error('end_date')
                                    <small class="form-text text-muted alert-danger messages-alert"> {{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class="form-control-label">Trạng thái</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check-inline form-check input-check">
                                    <input type="radio" id="inline-radio1" name="status" value="0"
                                        class="form-check-input" checked="checked">
                                    <label for="inline-radio1" class="form-check-label ">Không kích hoạt</label>
                                    <input type="radio" id="inline-radio2" name="status" value="1"
                                        class="form-check-input">
                                    <label for="inline-radio2" class="form-check-label ">Kích hoạt</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Lưu
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Làm mới
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

