@extends('backend.master')
@section('title', 'Danh sách Phiếu Giảm Giá')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35"></h3>
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <a href="{{ route('coupons.create') }}">
                        <button class="au-btn au-btn-icon btn-info au-btn--small"><i class="zmdi zmdi-plus"></i>Thêm mới</button>
                    </a>
                </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox"><span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>STT</th>
                            <th>Mã Coupon</th>
                            <th>Giá trị</th>
                            <th>Ngày bắt đầu</th>
                            <th>Ngày kết thúc</th>
                            <th>Trạng thái</th>
                            <th style="text-align: center;padding-right: 40px;">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($coupons as $key => $item)
                            <tr class="tr-shadow">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox">
                                        <span class="au-checkmark"></span>
                                    </label>
                                </td>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $item->code }}</td>
                                <td class="desc">{{ $item->value }}%</td>
                                <td>{{ \Carbon\Carbon::parse($item->start_date)->format('d/m/Y') }}</td>
                                <td>{{ \Carbon\Carbon::parse($item->end_date)->format('d/m/Y') }}</td>
                                <td>
                                    <label class="switch switch-3d switch-success mr-3">
                                        <input type="checkbox" data-id="{{ $item->id }}" data-url="{{ route('coupons.change.status', ['id' => $item->id]) }}" class="switch-input" @if ($item->status == 1) ?  checked="true" : checked="false" @endif  >
                                            <span class="switch-label unactive"></span>
                                            <span class="switch-handle"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <a class="editItem" href="{{ route('coupons.edit', ['id' => $item->id]) }}">
                                            <button class="item btn-edit" title="Chỉnh sửa"><i class="zmdi zmdi-edit"></i></button>
                                        </a>
                                        <a href="" class="deleteItem" data-url="{{ route('coupons.destroy',['id' => $item->id]) }}">
                                            <button class="item btn-delete" title="Xoá"><i class="zmdi zmdi-delete"></i></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $coupons->render('backend.layouts.pagination') }}
            </div>
        </div>
    </div>
@endsection
