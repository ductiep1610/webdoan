<!-- Tin nhắn thông báo -->
@if(session('success'))
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show messages-alert">
        <span class="badge badge-pill badge-success">Success</span>
        {{ session('success') }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif