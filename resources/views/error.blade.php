@if ($errors->any())
    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show messages-alert">
        <span class="badge badge-pill badge-danger">Error</span>
        @foreach ($errors->all() as $error)
        {{ $error }}
        @endforeach
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif