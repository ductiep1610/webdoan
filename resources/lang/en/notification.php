<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Notification Messages Lines
    |--------------------------------------------------------------------------
    |
    */

    'login_success' => 'Đăng nhập hệ thống thành công',
    'login_errors' => 'Đăng nhập không thành công. Email hoặc mật khẩu không chính xác',
    'add_success' => 'Thêm mới thành công',
    'update_success' => 'Cập nhật thành công',
    'change_success' => 'Thay đổi thành công',
    'delete_success' => 'Đã xoá bản ghi',
    'unactive_account' => 'Tài khoản chưa được kích hoạt. Liên hệ quản trị viên để được kích hoạt',
    'not_found' => 'Không tìm thấy dữ liệu',
    'can_not_delete' => 'Không thể xoá dữ liệu này',
    'not_forbidden' => 'Không có quyền xoá dữ liệu này',
    'approved_success' => 'Phê duyệt thành công',
]

?>