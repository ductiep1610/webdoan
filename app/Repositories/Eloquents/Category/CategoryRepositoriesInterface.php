<?php

namespace App\Repositories\Eloquents\Category;

use App\Repositories\RepositoriesInterface;

interface CategoryRepositoriesInterface extends RepositoriesInterface
{
    /**
     * Find by column
     * @param $attribute, $column
     * @return mixed
     */
    public function findBy($column, $attribute);
    
}