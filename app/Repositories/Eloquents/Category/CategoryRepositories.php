<?php

namespace App\Repositories\Eloquents\Category;

use App\Models\Category;
use App\Repositories\BaseRepositories;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class CategoryRepositories extends BaseRepositories implements CategoryRepositoriesInterface
{
    public function getModel()
    {
        return Category::class;
    }

    public function findBy($column, $attribute)
	{
		return $this->model->where($column, $attribute)->get();
	}

    public function create(array $data): Model
    {
        return Category::create([
            'name'=> $data['name'],
    		'parent_id'=> $data['sltCate'],
    		'slug'=> Str::slug($data['name']),
            'status' => $data['status'],
        ]);
    }

    public function update($id, array $data): bool
    {
        $category = Category::find($id);
        return $category->update([
            'name'=> $data['name'],
    		'parent_id'=> $data['sltCate'],
    		'slug'=> Str::slug($data['name']),
            'status' => $data['status'],
        ]);
    }

}