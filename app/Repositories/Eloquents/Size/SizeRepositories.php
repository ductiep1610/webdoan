<?php

namespace App\Repositories\Eloquents\Size;

use App\Models\Size;
use App\Repositories\BaseRepositories;

class SizeRepositories extends BaseRepositories implements SizeRepositoriesInterface
{
    public function getModel()
    {
        return Size::class;
    }
}