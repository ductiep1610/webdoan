<?php

namespace App\Repositories\Eloquents\Coupon;

use App\Models\Coupon;
use App\Repositories\BaseRepositories;
use Illuminate\Contracts\Pagination\Paginator;

class CouponRepositories extends BaseRepositories implements CouponRepositoriesInterface
{
    public function getModel()
    {
        return Coupon::class;
    }

    /**
     * Get list latest and paginate
     * @return void
     */
    public function getAll():Paginator
    {
        return $this->model->latest()->paginate(10);
    }
}