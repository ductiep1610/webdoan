<?php

namespace App\Repositories\Eloquents\Color;

use App\Models\Color;
use App\Repositories\BaseRepositories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\Paginator;

class ColorRepositories extends BaseRepositories implements ColorRepositoriesInterface
{
    public function getModel()
    {
        return Color::class;
    }

    public function create(array $data): Model
    {
        return Color::create([
            'name'=> ucwords($data['name']),
    		'code_color'=> ucwords($data['code_color']),
        ]);
    }

    // public function update($id, array $data): bool
    // {
    //     $color = Color::find($id);
    //     return $color->update([
    //         'name'=> ucwords($data['name']),
    // 		'code_color'=> ucwords($data['code_color']),
    //     ]);
    // }

    /**
     * Get list latest and paginate
     * @return void
     */
    public function getAll():Paginator
    {
        return $this->model->latest()->paginate(10);
    }
}