<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

interface RepositoriesInterface
{
    /**
     * Get all
     * @return Paginator
     */
    public function getAll(): Paginator;

    /**
     * Find By Id
     * @param $id
     * @return Model|null
     */
    public function find($id): Model|null;

    /**
     * Create a record
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model;

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @param array $data
     * @return boolean
     */
    public function update($id, array $data): bool;

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return boolean
     */
    public function delete($id): bool;
}