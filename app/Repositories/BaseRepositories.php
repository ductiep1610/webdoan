<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepositories implements RepositoriesInterface
{
    protected $model;
    const DEFAULT_PERPAGE = 10;

    public function __construct()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    abstract public function getModel();

    public function getAll(): Paginator
    {
        return $this->model->paginate(self::DEFAULT_PERPAGE);
    }

    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    public function find($id): Model|null
    {
        return $this->model->find($id);
    }

    public function update($id, array $data): bool
    {
        $result = $this->find($id);
        if ($result) {
            return $result->update($data);
        }

        return false;
    }

    public function delete($id): bool
    {
        $result = $this->find($id);
        if ($result) {
            return $result->delete();
        }

        return false;
    }
}
