<?php

namespace App\Traits;

trait UploadsImageTraits{

	public function storageTraitUpload($request, $fieldName, $folderName) {

		if ($request->hasFile($fieldName)){

			$file = $request->$fieldName;
	        $fileNameOrigin = $file->getClientOriginalName();
	        $fileName = time() . '_' . auth()->id() . '_' . $fileNameOrigin;
			$destinationPath = public_path('uploads/' . $folderName);
	        $filePath = $request->file($fieldName)->move($destinationPath, $fileName);
	        $dataUpload = [
				'file_name' => $fileName,
	            'file_path' => $filePath,
	        ];
	        return $dataUpload;
		}
		return null;
		
	}

	public function storageTraitUploadMultiple($file, $folderName) {

        $fileNameOrigin = $file->getClientOriginalName();
        $fileName = time() . '_' . auth()->id() . '_' . $fileNameOrigin;
		$filePath = $file->move('uploads/' . $folderName, $fileName);
        $dataUpload = [
            'file_name' => $fileName,
            'file_path' => $filePath,
        ];
        return $dataUpload;
	}

}