<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSizeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size' => 'unique:sizes,size|string',
        ];
    }

    public function messages()
    {
        return [
            'size.string' => 'Trường Size không được chứa ký tự đặc biệt',
            'size.unique' => 'Dữ liệu đã tồn tại',
        ];
    }
}
