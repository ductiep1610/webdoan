<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string',
            'value' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ];
    }

    public function messages()
    {
        return [
            'code.required'     => 'Chưa nhập mã giảm giá',
            'code.string'       => 'Ký tự nhập vào phải là một chuỗi',
            'value.required'    => 'Chưa nhập giá trị mã giảm giá',
            'value.numeric'     => 'Giá trị nhập vào phải là số',
            'start_date.required' => 'Ngày bắt đầu không được để trống',
            'start_date.date'   => 'Giá trị nhập vào phải là ngày/tháng/năm',
            'end_date.required' => 'Ngày kết thúc không được để trống',
            'end_date.date'     => 'Giá trị nhập vào phải là ngày/tháng/năm',
            'end_date.after_or_equal'    => 'Ngày kết thúc phải lớn hơn ngày bắt đầu',
        ];
    }
}
