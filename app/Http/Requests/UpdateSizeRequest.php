<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSizeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size' => "required|unique:sizes,size, $this->id",
        ];
    }

    public function messages()
    {
        return [
            'size.required' => 'Tên Size không được trống',
            'size.unique'  => 'Size đã tồn tại',
        ];
    }
}
