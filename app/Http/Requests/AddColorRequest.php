<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'string|unique:colors,name',
            'code_color' => 'string|unique:colors,code_color',
        ];
    }

    public function messages()
    {
        return [
            'name.unique'       => 'Dữ liệu đã tồn tại',
            'code_color.unique' => 'Mã màu đã tồn tại',
        ];
    }
}
