<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'unique:products,product_name',
            'price' => 'numeric',
            'promotion' => 'numeric',
            'image' => 'required|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png,image/jpg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'Tên đã tồn tại',
            'price.numeric' => 'Dữ liệu nhập phải là một số',
            'promotion.numeric' => 'Dữ liệu nhập phải là một số',
            'image.required' => 'Tệp không được để trống',
            'image.image' => 'Tệp dữ liệu phải là ảnh',
            'image.mimes' => 'Tệp phải có đuôi .jpeg,jpg,png',
            'image.max' => 'Tệp quá kích thước cho phép',
        ];
    }
}
