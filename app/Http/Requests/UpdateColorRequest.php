<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name-edit' => "unique:colors,name, $this->id",
            'code_color-edit' => "unique:colors,code_color, $this->id",
        ];
    }

    public function messages()
    {
        return [
            'name-edit.unique' => 'Dữ liệu đã tồn tại',
            'code_color-edit.unique'  => 'Mã màu đã tồn tại',
        ];
    }
}
