<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddProductRequest;
use App\Models\Admin;
use App\Models\ImageProduct;
use App\Repositories\Eloquents\Category\CategoryRepositoriesInterface;
use App\Traits\UploadsImageTraits;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    use UploadsImageTraits;

    private $product;
    private $cateRepo;
    private $imageProduct;

    public function __construct(CategoryRepositoriesInterface $cateRepo, Product $product, ImageProduct $imageProduct)
    {
        $this->cateRepo = $cateRepo;
        $this->product = $product;
        $this->imageProduct = $imageProduct;
    }

    /**
     * Danh sách sản phẩm chưa được duyệt
     *
     * @return void
     */
    public function index()
    {
        $products = $this->product->where('status', Product::STATUS_DEFAULT)->latest()->get();
        return view('backend.products.index', compact('products'));
    }

    /**
     * Danh sách sản phẩm đã được duyệt
     *
     * @return void
     */
    public function listApproved()
    {
        $products = $this->product->WhereIn('status', [Product::STATUS_ACTIVE, Product::STATUS_HIDEN])->latest()->get();
        return view('backend.products.list-approved', compact('products'));
    }

    
    public function create()
    {
        $cates = $this->cateRepo->getAll();
        return view('backend.products.add', compact('cates'));
    }

    public function store(AddProductRequest $request)
    {
        $dataProduct = [
            'product_name'=> ucwords($request->name),
            'slug'=> Str::slug($request->name),
            'cate_id'=> $request->slCate,
            'price'=> $request->price,
            'promotion'=> $request->promotion,
            'description'=> $request->description,
            'admin_id'=> auth()->id()
        ];
        $dataUploadImage = $this->storageTraitUpload($request, 'image', 'products');
        if(!empty($dataUploadImage)) {
            $dataProduct['image'] = $dataUploadImage['file_name'];
        }

        $product = $this->product->create($dataProduct);

        //Insert data to image_details
        if($request->hasFile('image_detail')) {
            foreach($request->image_detail as $fileitem) {
                $dataProductImageDetail = $this->storageTraitUploadMultiple($fileitem, 'imagesdetail');
                $product->images()->create([
                    'image_detail' => $dataProductImageDetail['file_name']
                ]);
            }
        }
        return redirect()->route('products.index')->with('success', __('notification.add_success'));
    }

    /**
     * Hiển thị chi tiết sản phẩm
     *
     * @param [type] $id
     * @return void
     */
    public function show($id)
    {
        $product = $this->product->find($id);
        return view('backend.products.detail-product', compact('product'));
    }

    /**
     * Hiển thị form sửa sản phẩm
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $product = $this->product->find($id);
        $cates = $this->cateRepo->getAll();
        $imagesDetail = $this->imageProduct->where('product_id', $product->id)->get();
        return view('backend.products.edit', compact('product', 'cates', 'imagesDetail'));
    }

    /**
     * Xoá ảnh chi tiết sản phẩm
     *
     * @param [type] $id
     * @param [type] $id_image
     * @return void
     */
    public function getdeleteImageDetail($id, $id_image)
    {
        $image_detail = $this->imageProduct->where('product_id', $id)->where('id', $id_image)->first();
        $image_detail->delete();
        return response()->json([
            'message' => __('notification.delete_success')
        ], Response::HTTP_OK);
    }

    /**
     * Cập nhật sản phẩm
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $dataProduct = [
            'product_name'=> ucwords($request->name),
            'slug'=> Str::slug($request->name),
            'cate_id'=> $request->slCate,
            'price'=> $request->price,
            'promotion'=> $request->promotion,
            'description'=> $request->description,
            'admin_id'=> auth()->id()
        ];

        $dataUploadImage = $this->storageTraitUpload($request, 'image', 'products');
        if(!empty($dataUploadImage)) {
            $dataProduct['image'] = $dataUploadImage['file_name'];
        }
        $this->product->find($id)->update($dataProduct);
        $product = $this->product->find($id);

        //Insert data to image_details
        if($request->hasFile('image_detail')) {
            foreach($request->image_detail as $fileitem) {
                $dataProductImageDetail = $this->storageTraitUploadMultiple($fileitem, 'imagesdetail');
                $product->images()->create([
                    'image_detail' => $dataProductImageDetail['file_name']
                ]);
            }
        }
        return redirect()->back()->with('success', __('notification.update_success'));
    }

    /**
     * Xoá sản phẩm chưa duyệt
     * @param [type] $id
     * @return void
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);

        if ($product) {
            if ($product->status == Product::STATUS_DEFAULT) {
                $product->delete();
                return response()->json([
                    'message' => __('notification.delete_success')
                ], Response::HTTP_OK);
            }            
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Xoá sản phẩm trong danh sách đã duyệt
     *
     * @param [type] $id
     * @return void
     */
    public function getDeleteApproved($id)
    {
        if(auth()->guard('admin')->user()->level == Admin::MEMBER){
            return response()->json([
                'message' => __('notification.not_forbidden')
            ], Response::HTTP_FORBIDDEN);
        }
        $product = $this->product->find($id);

        if ($product) {
            $product->delete();
            return response()->json([
                'message' => __('notification.delete_success')
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Thay đổi trạng thái sản phẩm
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function changeStatus(Request $request, $id)
    {
        $this->product->find($id)->update(['status' => $request->status]);
        return response()->json([
            'message' => __('notification.change_success')
        ], Response::HTTP_OK);
    }

    /**
     * Duyệt sản phẩm
     *
     * @param [type] $id
     * @return void
     */
    public function getbrowseProduct($id)
    {
        if(auth()->guard('admin')->user()->level == Admin::MEMBER){
            return response()->json([
                'message' => __('notification.not_forbidden')
            ], Response::HTTP_FORBIDDEN);
        }
        $product = $this->product->find($id);

        if ($product) {
            $product->update(['status' => Product::STATUS_ACTIVE]);
            return response()->json([
                'message' => __('notification.approved_success')
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }
}
