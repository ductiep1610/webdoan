<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\Eloquents\Category\CategoryRepositoriesInterface;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    private $categoryRepo;

	public function __construct(CategoryRepositoriesInterface $categoryRepo)
	{
		$this->categoryRepo = $categoryRepo;
	}

    /**
     * Display a listing of the resource.
     * Show list category item
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cates = $this->categoryRepo->getAll();
        return view('backend.categories.index', compact('cates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cates = $this->categoryRepo->getAll();
        return view('backend.categories.add', compact('cates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryRequest $request)
    {
        $data = $request->all();

        $this->categoryRepo->create($data);

        return redirect()->route('category.index')->with('success', __('notification.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($cate_slug)
    {   
        $cates = $this->categoryRepo->getAll();
        $data = $this->categoryRepo->findBy('slug', $cate_slug)->first();
        return view('backend.categories.edit', compact('cates', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = $this->categoryRepo->find($id);
        if(!$category) {
            return redirect()->back()
                ->withErrors('error', __('notification.not_found'));
        }
        
        $data = $request->all();
        $this->categoryRepo->update($id,$data);

        return redirect()->route('category.index')
            ->with('success', __('notification.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->guard('admin')->user()->level == 2){
            return response()->json([
                'message' => 'Forbidden'
            ], Response::HTTP_FORBIDDEN);
        }

        $this->categoryRepo->delete($id);

        return response()->json([
            'message' => __('notification.delete_success')
        ], Response::HTTP_OK);
    }

    public function changeStatus(Request $request, $id)
    {
        $this->categoryRepo->find($id)->update(['status' => $request->status]);
        return response()->json([
            'message' => __('notification.change_success')
        ], Response::HTTP_OK);
    }
}
