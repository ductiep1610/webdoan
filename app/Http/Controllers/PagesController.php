<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     *  Home Page
     */
    public function index()
    {
        return view('frontend.pages.home');
    }

    /**
     *  Blog Page
     */
    public function getBlogPage()
    {
        return view('frontend.pages.blog');
    }

    /**
     *  Blog Detail Page
     */
    public function getBlogDetailPage()
    {
        return view('frontend.pages.blog-detail');
    }

    /**
     *  Contact Page
     */
    public function getContactPage()
    {
        return view('frontend.pages.contact');
    }

    /**
     *  Product Page
     */
    public function getProductPage()
    {
        return view('frontend.pages.product');
    }

    /**
     *  Shop Page
     */
    public function getShopPage()
    {
        return view('frontend.pages.shop');
    }

    /**
     *  Login Page
     */
    public function getLoginPage()
    {
        return view('frontend.pages.login');
    }

    /**
     *  Register Page
     */
    public function getRegisterPage()
    {
        return view('frontend.pages.register');
    }
}
