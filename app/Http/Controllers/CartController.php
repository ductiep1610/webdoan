<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Get list product in Cart
     */
    public function index()
    {
        return view('frontend.pages.cart');
    }
}
