<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AddAccountRequest;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    private $account;

    public function __construct(Admin $account)
    {
        $this->account = $account;  
    }

    /**
     * Show List Account
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = $this->account->all();
        return view('backend.accounts.index', compact('accounts'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $account =  $this->account->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => $request->level,
            'status' => $request->status
        ]);
        return response()->json([
            'data' => $account,
            'message' => __('notification.add_success')
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = $this->account->find($id);
        if ($account) {
            return response()->json([
                'data' => $account
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $account = $this->account->find($request->id);

        if ($account) {
            $dataUpdate = $request->only('name', 'level');
            $account = $account->update($dataUpdate);

            return response()->json([
                'data' => $account,
                'message' => __('notification.update_success')
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->account->find($id)->delete();
        return response()->json([
            'message' => __('notification.delete_success')
        ], Response::HTTP_OK);
    }

    public function changeStatus(Request $request, $id)
    {
        $account = $this->account->find($id);
        if($account) {
            $account->update(['status' => $request->status]);
            return response()->json([
                'message' => __('notification.change_success')
            ], Response::HTTP_OK);
        }
        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }
}
