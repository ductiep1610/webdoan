<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductVariant;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\Eloquents\Size\SizeRepositoriesInterface;
use App\Repositories\Eloquents\Color\ColorRepositoriesInterface;

class ProductVariantController extends Controller
{
    private $product;
    private $color;
    private $size;
    private $productVariant;

    public function __construct(
        Product $product, 
        ProductVariant $productVariant, 
        ColorRepositoriesInterface $color, 
        SizeRepositoriesInterface $size
    )
    {
        $this->product = $product;
        $this->productVariant = $productVariant;
        $this->color = $color;
        $this->size = $size;
    }

    public function index($product_id)
    {
        $product = $this->product->find($product_id);
        $productVariants = $product->productVariants;
        $colors = $this->color->getAll();
        $sizes = $this->size->getAll();
        return view('backend.productvariants.index', compact('product', 'productVariants', 'colors', 'sizes'));
    }

    public function store(Request $request)
    {
        $product = $this->product->find($request->product_id);
        $data = $request->all();
        unset($data['_token']);
        $this->productVariant->updateOrCreate($data);
        $this->updateQty($request->product_id);
        return redirect()->route('variants.index', $product)->with('success', __('notification.add_success'));
    }

    public function edit($product_id, $id)
    {
        $product = $this->product->find($product_id);
        $productVariant = $this->productVariant->find($id);
        $colors = $this->color->getAll();
        $sizes = $this->size->getAll();
        return view('backend.productvariants.edit', compact('product', 'productVariant', 'colors', 'sizes'));
    }

    public function update(Request $request, $product_id, $id)
    {
        $product = $this->product->find($request->product_id);
        $data = $request->all();
        $this->productVariant->find($id)->update($data);
        $this->updateQty($product_id);
        return redirect()->route('variants.index', $product)->with('success', __('notification.update_success'));
    }

    public function destroy($product_id, $id)
    {
        $productVariant = $this->productVariant->find($id);
        if ($productVariant) {
            $productVariant->delete();
            $this->updateQty($product_id);

            return response()->json([
                'message' => __('notification.delete_success')
            ], Response::HTTP_OK);
        } 
        
        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Cập nhật số lượng sản phẩm
     *
     * @return void
     */
    public function updateQty($product_id)
    {
        $product = $this->product->find($product_id);
        $productVariants = $product->productVariants;
        $totalQty = array_sum(array_column($productVariants->toArray(), 'qty'));
        $product->update(['qty' => $totalQty]);
    }
}
