<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddSizeRequest;
use App\Http\Requests\UpdateSizeRequest;
use Illuminate\Http\Request;
use App\Repositories\Eloquents\Size\SizeRepositoriesInterface;
use Symfony\Component\HttpFoundation\Response;

class SizeController extends Controller
{
    private $sizeRepo;

    public function __construct(SizeRepositoriesInterface $sizeRepo)
    {
        $this->sizeRepo = $sizeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sizes = $this->sizeRepo->getAll();
        return view('backend.sizes.index', compact('sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddSizeRequest $request
     * @return void
     */
    public function store(AddSizeRequest $request)
    {
        $size = $this->sizeRepo->create([
            'size' => $request->size,
        ]);

        return response()->json([
            'data' => $size,
            'message' => __('notification.add_success')
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @return void
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $size = $this->sizeRepo->find($id);
        if ($size) {
            return response()->json([
                'data' => $size
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSizeRequest $request
     * @return void
     */
    public function update(UpdateSizeRequest $request)
    {
        $size = $this->sizeRepo->find($request->id);

        if ($size) {
            $dataUpdate = $request->only('size');
            $size = $size->update($dataUpdate);

            return response()->json([
                'data' => $size,
                'message' => __('notification.update_success')
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->sizeRepo->find($id)->delete();
        return response()->json([
            'message' => __('notification.delete_success')
        ], Response::HTTP_OK);
    }
}
