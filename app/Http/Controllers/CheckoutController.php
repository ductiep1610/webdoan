<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    /**
     *  Checkout Page
     */
    public function index()
    {
        return view('frontend.pages.checkout');
    }
}
