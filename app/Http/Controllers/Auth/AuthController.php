<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    /**
     * Show Login Admin Form
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('backend.login');
    }

    /**
     * Login Admin
     * 
     * @return \Illuminate\Http\Response
     */
    public function postlogin(Request $request)
    {
        $remember = $request->has('remember') ? true : false;

        if (auth()->guard('admin')->attempt([
            'email' => $request->email,
            'password' => $request->password,
            'status' => Admin::ACTIVE
        ], $remember)) {
            return redirect()->route('admin.dashboard')->with('success', __('notification.login_success'));
        } elseif (auth()->guard('admin')->attempt([
            'email' => $request->email, 
            'password' => $request->password, 
            'status' => Admin::UNACTIVE], $remember)
            ) {
            return redirect()->back()->withErrors([__('notification.unactive_account')]);
        }

        return redirect()->back()->withErrors([__('notification.login_errors')]);
    }

    /**
     * Logout Admin
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('login');
    }
}
