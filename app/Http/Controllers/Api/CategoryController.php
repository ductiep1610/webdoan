<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function getCategoriesList()
    {
        $categories = $this->categoryService->listCategories();
        
        return response()->json([
            'message' => 'Resource found',
            'data' => $categories
        ], Response::HTTP_OK);
    }
}
