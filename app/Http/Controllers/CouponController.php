<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCouponRequest;
use App\Repositories\Eloquents\Coupon\CouponRepositoriesInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CouponController extends Controller
{
    private $couponRepository;

    public function __construct(CouponRepositoriesInterface $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = $this->couponRepository->getAll();
        return view('backend.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.coupons.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCouponRequest $request)
    {
        $this->couponRepository->create([
            'code' => $request->code,
            'value' => $request->value,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'status' => $request->status
        ]);
        
        return redirect()->route('coupons.index')->with('success', __('notification.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $coupon = $this->couponRepository->find($id);
        return view('backend.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->couponRepository->find($id)->update([
            'code' => $request->code,
            'value' => $request->value,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'status' => $request->status
        ]);

        return redirect()->route('coupons.index')->with('success', __('notification.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->couponRepository->find($id)->delete();
        return response()->json([
            'message' => __('notification.delete_success')
        ], Response::HTTP_OK);
    }

    public function changeStatus(Request $request, $id)
    {
        $this->couponRepository->find($id)->update(['status' => $request->status]);
        return response()->json([
            'message' => __('notification.change_success')
        ], Response::HTTP_OK);
    }
}
