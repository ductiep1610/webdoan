<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddColorRequest;
use App\Http\Requests\UpdateColorRequest;
use App\Repositories\Eloquents\Color\ColorRepositoriesInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ColorController extends Controller
{
    private $colorRepo;

    public function __construct(ColorRepositoriesInterface $colorRepo)
    {
        $this->colorRepo = $colorRepo;
    }

    /**
     * Show list colors
     *
     * @return void
     */
    public function index()
    {
        $colors = $this->colorRepo->getAll();
        return view('backend.colors.index', compact('colors'));
    }

    /**
     * Add Color
     *
     * @param AddColorRequest $request
     * @return void
     */
    public function store(AddColorRequest $request)
    {
        $data = $request->all();

        $this->colorRepo->create($data);

        return redirect()->route('colors.index')->with('success', __('notification.add_success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $color = $this->colorRepo->find($id);
        if ($color) {
            return response()->json([
                'data' => $color
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateColorRequest $request
     * @return void
     */
    public function update(UpdateColorRequest $request)
    {
        $color = $this->colorRepo->find($request->id);
        if($color) {
            $data = $request->all();
            $color = $color->update($data);
            
            return response()->json([
                'data' => $color,
                'message' => __('notification.update_success')
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => __('notification.not_found')
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Delete color by id
     *
     * @param [type] $id
     * @return void
     */
    public function destroy($id)
    {
        $this->colorRepo->find($id)->delete();
        return response()->json([
            'message' => __('notification.delete_success')
        ], Response::HTTP_OK);
    }

}
