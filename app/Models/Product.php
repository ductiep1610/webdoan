<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const STATUS_DEFAULT = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_HIDEN = 0;

    protected $fillable = [
        'product_name',
        'cate_id',
        'price',
        'promotion',
        'description',
        'image',
        'slug',
        'qty',
        'focus',
        'status',
        'amount_sold',
        'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'cate_id');
    }

    public function images()
    {
        return $this->hasMany(ImageProduct::class,'product_id');
    }

    public function productVariants()
    {
        return $this->hasMany(ProductVariant::class,'product_id');
    }

    public function getImageUrlAttribute()
    {
        return $this->image ? asset('uploads/products/' . $this->image) : '';
    }

    public function getFormatPriceAttribute()
    {
        return number_format($this->price, 0, ',','.');
    }

    public function getFormatPromotionAttribute()
    {
        return number_format($this->price*(100 - $this->promotion)/100, 0, ',','.');
    }
}
