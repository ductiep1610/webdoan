<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    const ACTIVE = 1;
    const UNACTIVE = 0;

    protected $fillable = ['code', 'value', 'start_date', 'end_date', 'status'];

}
