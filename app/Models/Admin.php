<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ACTIVE = 1;
    const UNACTIVE = 0;

    const ADMIN = 2;
    const MEMBER = 1;

    protected $table = 'admins';
    
    protected $guard = 'admin';

    protected $fillable = [
        'name', 'email', 'password', 'level', 'status', 'phone', 'address'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getStatusActiveAttribute()
    {
        if ($this->status == Category::ACTIVE) {
            return '<span class="status--process">Kích hoạt</span>';
        } else {
            return '<span class="status--denied">Không kích hoạt</span>';
        }
    }
}
