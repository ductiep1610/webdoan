<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    const ACTIVE = 1;
    const UNACTIVE = 0;

    protected $fillable = [
        'name', 'slug', 'parent_id', 'status',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getStatusActiveAttribute()
    {
        if ($this->status == Category::ACTIVE) {
            return '<span class="status--process">Kích hoạt</span>';
        } else {
            return '<span class="status--denied">Không kích hoạt</span>';
        }
    }
}
