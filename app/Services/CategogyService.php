<?php

namespace App\Services;

use App\Repositories\Eloquents\Category\CategoryRepositoriesInterface;

class CategoryService extends BaseService
{

    protected $categoryRepository;
    public function __construct(CategoryRepositoriesInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function listCategories()
    {
        return $this->categoryRepository->getAll();
    }
}