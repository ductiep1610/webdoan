<?php

namespace App\Providers;

use App\Repositories\Eloquents\Category\CategoryRepositories;
use App\Repositories\Eloquents\Category\CategoryRepositoriesInterface;
use App\Repositories\Eloquents\Color\ColorRepositories;
use App\Repositories\Eloquents\Color\ColorRepositoriesInterface;
use App\Repositories\Eloquents\Coupon\CouponRepositories;
use App\Repositories\Eloquents\Coupon\CouponRepositoriesInterface;
use App\Repositories\Eloquents\Size\SizeRepositories;
use App\Repositories\Eloquents\Size\SizeRepositoriesInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            CategoryRepositoriesInterface::class,
            CategoryRepositories::class,
        );

        $this->app->singleton(
            CouponRepositoriesInterface::class,
            CouponRepositories::class
        );

        $this->app->singleton(
            SizeRepositoriesInterface::class,
            SizeRepositories::class
        );

        $this->app->singleton(
            ColorRepositoriesInterface::class,
            ColorRepositories::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
