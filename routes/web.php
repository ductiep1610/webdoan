<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductVariantController;
use App\Http\Controllers\SizeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Tài khoản đăng nhập
 *  Email: ductiep1610@gmail.com
 *  Password: 16101996
 */
Route::get('/cms', [AuthController::class, 'login'])->name('login');
Route::post('/cms', [AuthController::class, 'postlogin'])->name('post.login-admin');
Route::get('/logout', [AuthController::class, 'logout'])->name('post.logout-admin');
Route::group(['prefix' => 'admins', 'middleware' => ['auth:admin']], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.dashboard');

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('category.index');
        Route::get('/add', [CategoryController::class, 'create'])->name('category.create');
        Route::post('/add', [CategoryController::class, 'store'])->name('category.store');
        Route::get('/edit/{cate_slug}', [CategoryController::class, 'edit'])->name('category.edit');
        Route::post('/update/{id}', [CategoryController::class, 'update'])->name('category.update');
        Route::get('/delete/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');
        Route::get('/change/{id}', [CategoryController::class, 'changeStatus'])->name('category.change.status');
    });

    Route::group(['prefix' => 'accounts'], function () {
        Route::get('/', [AccountController::class, 'index'])->name('accounts.index');
        Route::post('/add', [AccountController::class, 'store'])->name('accounts.store');
        Route::get('/edit/{id}', [AccountController::class, 'edit'])->name('accounts.edit');
        Route::post('/update', [AccountController::class, 'update'])->name('accounts.update');
        Route::get('/delete/{id}', [AccountController::class, 'destroy'])->name('accounts.destroy');
        Route::get('/change/{id}', [AccountController::class, 'changeStatus'])->name('accounts.change.status');
    });

    Route::group(['prefix' => 'coupons'], function () {
        Route::get('/', [CouponController::class, 'index'])->name('coupons.index');
        Route::get('/add', [CouponController::class, 'create'])->name('coupons.create');
        Route::post('/add', [CouponController::class, 'store'])->name('coupons.store');
        Route::get('/edit/{id}', [CouponController::class, 'edit'])->name('coupons.edit');
        Route::post('/update/{id}', [CouponController::class, 'update'])->name('coupons.update');
        Route::get('/delete/{id}', [CouponController::class, 'destroy'])->name('coupons.destroy');
        Route::get('/change/{id}', [CouponController::class, 'changeStatus'])->name('coupons.change.status');
    });

    Route::group(['prefix' => 'sizes'], function () {
        Route::get('/', [SizeController::class, 'index'])->name('sizes.index');
        Route::post('/add', [SizeController::class, 'store'])->name('sizes.store');
        Route::get('/edit/{id}', [SizeController::class, 'edit'])->name('sizes.edit');
        Route::post('/update', [SizeController::class, 'update'])->name('sizes.update');
        Route::get('/delete/{id}', [SizeController::class, 'destroy'])->name('sizes.destroy');
    });

    Route::group(['prefix' => 'colors'], function () {
        Route::get('/', [ColorController::class, 'index'])->name('colors.index');
        Route::post('/add', [ColorController::class, 'store'])->name('colors.store');
        Route::get('/edit/{id}', [ColorController::class, 'edit'])->name('colors.edit');
        Route::post('/update', [ColorController::class, 'update'])->name('colors.update');
        Route::get('/delete/{id}', [ColorController::class, 'destroy'])->name('colors.destroy');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/not-approved', [ProductController::class, 'index'])->name('products.index');
        Route::get('/approved', [ProductController::class, 'listApproved'])->name('products.listApproved');
        Route::get('/add', [ProductController::class, 'create'])->name('products.create');
        Route::post('/add', [ProductController::class, 'store'])->name('products.store');
        Route::get('/show/{id}', [ProductController::class, 'show'])->name('products.show');
        Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('products.edit');
        Route::post('/update/{id}', [ProductController::class, 'update'])->name('products.update');
        Route::get('/delete/not-approved/{id}', [ProductController::class, 'destroy'])->name('products.destroy');
        Route::get('/delete/approved/{id}', [ProductController::class, 'getDeleteApproved'])->name('products.getDeleteApproved');
        Route::get('/delete/{id}/{id_image}', [ProductController::class, 'getdeleteImageDetail'])->name('products.getdelImageDetail');
        Route::get('/change/{id}', [ProductController::class, 'changeStatus'])->name('products.change.status');
        Route::get('/browse-product/{id}', [ProductController::class, 'getbrowseProduct'])->name('products.getbrowseProduct');
    });

    Route::group(['prefix' => 'product/variants'], function () {
        Route::get('/{product_id}', [ProductVariantController::class, 'index'])->name('variants.index');
        Route::post('/add', [ProductVariantController::class, 'store'])->name('variants.store');
        Route::get('/{product_id}/edit/{id}', [ProductVariantController::class, 'edit'])->name('variants.edit');
        Route::post('{product_id}/update/{id}', [ProductVariantController::class, 'update'])->name('variants.update');
        Route::get('{product_id}/delete/{id}', [ProductVariantController::class, 'destroy'])->name('variants.destroy');
    });
});

Route::get('/', [PagesController::class, 'index'])->name('home.index');
Route::get('/blog', [PagesController::class, 'getBlogPage'])->name('blog.index');
Route::get('/blog/name', [PagesController::class, 'getBlogDetailPage'])->name('blog_detail.index');
Route::get('/contact', [PagesController::class, 'getContactPage'])->name('contact.index');
Route::get('/product', [PagesController::class, 'getProductPage'])->name('product.index');
Route::get('/shop', [PagesController::class, 'getShopPage'])->name('shop.index');
Route::get('/login', [PagesController::class, 'getLoginPage'])->name('login.index');
Route::get('/register', [PagesController::class, 'getRegisterPage'])->name('register.index');
Route::get('/shopping-cart', [CartController::class, 'index'])->name('cart.index');
Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout.index');