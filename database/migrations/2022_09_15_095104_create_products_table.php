<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->string('slug');
            $table->unsignedBigInteger('cate_id');
            $table->double('price');
            $table->tinyInteger('promotion');
            $table->longText('description')->nullable();
            $table->string('image');
            $table->tinyInteger('status')->default('2');
            $table->integer('qty')->default('0');
            $table->tinyInteger('focus')->default('0');
            $table->integer('amount_sold')->default('0');
            $table->integer('admin_id');
            $table->foreign('cate_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
