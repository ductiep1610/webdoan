/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : ecommerce

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 04/04/2023 19:35:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int NOT NULL,
  `status` int NOT NULL,
  `phone` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admins_email_unique`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (6, 'Nguyễn Đức Tiệp', 'ductiep1610@gmail.com', '$2y$10$RLFFVuIPK3I3Ex5xtBtolOXCRRcspdmIb0SH6LJVrd8D19AZK9VCq', 2, 1, NULL, NULL, NULL, 'IldBXLg3QXgm2krevuKw0CC3U2wBjKL0Hu1vcwKDQY3KklNqZtDq60amPHIV', '2022-08-29 08:21:38', '2023-02-02 01:49:24');
INSERT INTO `admins` VALUES (7, 'Lưu Sở Điềm', 'liuchutian0509@gmail.com', '$2y$10$WZCApv2Hv27Ey2nQG1rdaOwZXJgQiCxFKB2ARWhDw0HUIXAloYpOm', 1, 1, NULL, NULL, NULL, NULL, '2022-08-29 08:26:54', '2022-09-21 18:44:07');
INSERT INTO `admins` VALUES (28, 'admin', 'admin@gmail.com', '$2y$10$VdIhTj64w0C9910p9sLcQuvZXmZot.b1Y6c0WdomRmOFAZIrWw7Va', 2, 1, NULL, NULL, NULL, NULL, '2022-09-13 09:37:15', '2023-02-02 02:17:58');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int NOT NULL DEFAULT 0,
  `status` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Thời Trang Nữ', 'thoi-trang-nu', 0, 1, '2022-08-28 23:41:45', '2022-09-11 09:24:57');
INSERT INTO `categories` VALUES (2, 'Thời Trang Nam', 'thoi-trang-nam', 0, 1, '2022-08-28 23:42:03', '2022-09-18 08:42:03');
INSERT INTO `categories` VALUES (9, 'Quần Jean', 'quan-jean', 2, 0, '2022-08-30 15:24:58', '2022-09-20 06:30:34');
INSERT INTO `categories` VALUES (15, 'Áo Hoodie', 'ao-hoodie', 1, 0, '2022-09-13 10:32:33', '2022-09-20 06:31:08');
INSERT INTO `categories` VALUES (17, 'Áo thun', 'ao-thun', 1, 0, '2022-09-18 06:02:35', '2022-09-20 06:30:36');

-- ----------------------------
-- Table structure for colors
-- ----------------------------
DROP TABLE IF EXISTS `colors`;
CREATE TABLE `colors`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of colors
-- ----------------------------
INSERT INTO `colors` VALUES (4, 'Màu Cam', '#FF4500', '2022-09-14 08:30:20', '2022-09-14 10:32:41');
INSERT INTO `colors` VALUES (6, 'Màu Vàng', '#FFFF00', '2022-09-14 08:31:37', '2022-09-14 10:01:32');
INSERT INTO `colors` VALUES (7, 'Màu Hồng', '#FFC0CB', '2022-09-14 09:08:55', '2022-09-14 09:08:55');
INSERT INTO `colors` VALUES (12, 'Màu Xanh', '#3CB371', '2022-09-14 10:35:06', '2022-09-14 10:35:06');
INSERT INTO `colors` VALUES (14, 'Màu Đen', 'Black', '2022-09-14 18:51:18', '2022-09-14 19:03:36');
INSERT INTO `colors` VALUES (15, 'Màu Tím', 'MediumPurple', '2022-09-14 18:51:54', '2022-09-14 19:04:47');

-- ----------------------------
-- Table structure for coupons
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupons
-- ----------------------------
INSERT INTO `coupons` VALUES (3, 'TRUNGTHU2022', '30', '2022-09-01', '2022-09-15', 1, '2022-09-10 04:27:28', '2022-09-10 04:27:28');
INSERT INTO `coupons` VALUES (5, 'CKTG2022', '50', '2022-09-29', '2022-11-30', 0, '2022-09-11 23:56:27', '2022-09-11 23:56:42');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for image_products
-- ----------------------------
DROP TABLE IF EXISTS `image_products`;
CREATE TABLE `image_products`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint UNSIGNED NOT NULL,
  `image_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `image_products_product_id_foreign`(`product_id` ASC) USING BTREE,
  CONSTRAINT `image_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of image_products
-- ----------------------------
INSERT INTO `image_products` VALUES (1, 1, '1663627945_7_ao-so-mi-trang-dai-tay 2_.jpg', '2022-09-20 05:52:25', '2022-09-20 05:52:25');
INSERT INTO `image_products` VALUES (2, 1, '1663627945_7_ao-so-mi-trang-dai-tay-1.jpg', '2022-09-20 05:52:25', '2022-09-20 05:52:25');
INSERT INTO `image_products` VALUES (4, 3, '1663630349_7_aothuntronnutayngan2.jpeg', '2022-09-20 06:32:29', '2022-09-20 06:32:29');
INSERT INTO `image_products` VALUES (5, 3, '1663630349_7_aothuntronnutayngan3.jpeg', '2022-09-20 06:32:29', '2022-09-20 06:32:29');
INSERT INTO `image_products` VALUES (8, 3, '1663760487_7_aothuntronnutayngan4.jpeg', '2022-09-21 18:41:27', '2022-09-21 18:41:27');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (12, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (13, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (16, '2022_08_26_062255_create_admins_table', 1);
INSERT INTO `migrations` VALUES (17, '2022_08_27_022023_create_categories_table', 1);
INSERT INTO `migrations` VALUES (18, '2022_09_05_080704_create_coupons_table', 2);
INSERT INTO `migrations` VALUES (19, '2022_09_13_105731_create_sizes_table', 3);
INSERT INTO `migrations` VALUES (20, '2022_09_14_060537_create_colors_table', 4);
INSERT INTO `migrations` VALUES (30, '2022_09_15_095104_create_products_table', 5);
INSERT INTO `migrations` VALUES (31, '2022_09_17_220532_create_image_products_table', 5);
INSERT INTO `migrations` VALUES (32, '2022_09_19_151234_create_product_variants_table', 5);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token` ASC) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type` ASC, `tokenable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for product_variants
-- ----------------------------
DROP TABLE IF EXISTS `product_variants`;
CREATE TABLE `product_variants`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint UNSIGNED NOT NULL,
  `color_id` bigint UNSIGNED NOT NULL,
  `size_id` bigint UNSIGNED NOT NULL,
  `qty` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_variants_product_id_foreign`(`product_id` ASC) USING BTREE,
  INDEX `product_variants_color_id_foreign`(`color_id` ASC) USING BTREE,
  INDEX `product_variants_size_id_foreign`(`size_id` ASC) USING BTREE,
  CONSTRAINT `product_variants_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_variants_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `product_variants_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_variants
-- ----------------------------
INSERT INTO `product_variants` VALUES (1, 1, 15, 2, 20, '2022-09-20 13:28:23', '2022-09-20 13:28:23');
INSERT INTO `product_variants` VALUES (2, 1, 12, 1, 20, '2022-09-20 13:30:14', '2022-09-21 18:37:42');
INSERT INTO `product_variants` VALUES (4, 1, 6, 2, 30, '2022-09-20 13:35:43', '2022-09-20 13:35:43');
INSERT INTO `product_variants` VALUES (6, 3, 12, 2, 20, '2022-09-20 13:44:03', '2022-09-20 13:44:03');
INSERT INTO `product_variants` VALUES (7, 3, 6, 1, 10, '2022-09-20 13:44:32', '2022-09-20 13:44:32');
INSERT INTO `product_variants` VALUES (9, 1, 4, 3, 20, '2022-09-21 18:01:45', '2022-09-21 18:37:17');
INSERT INTO `product_variants` VALUES (15, 1, 7, 1, 20, '2022-09-21 18:32:47', '2022-09-21 18:32:47');
INSERT INTO `product_variants` VALUES (16, 3, 15, 2, 20, '2022-09-21 18:39:24', '2022-09-21 18:39:24');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate_id` bigint UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `promotion` tinyint NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT 2,
  `qty` int NOT NULL DEFAULT 0,
  `focus` tinyint NOT NULL DEFAULT 0,
  `amount_sold` int NOT NULL DEFAULT 0,
  `admin_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `products_cate_id_foreign`(`cate_id` ASC) USING BTREE,
  CONSTRAINT `products_cate_id_foreign` FOREIGN KEY (`cate_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'Áo Sơ Mi Công Sở', 'ao-so-mi-cong-so', 1, 100000, 10, '<p>Áo sơ mi c&ocirc;ng sở</p>', '1663627945_7_ao-so-mi-trang-dai-tay.jpg', 1, 110, 0, 0, 7, '2022-09-20 05:52:25', '2023-03-16 15:20:24');
INSERT INTO `products` VALUES (3, 'Áo Hoodie', 'ao-hoodie', 15, 200000, 1, '<p>ưdqwdqwdqwd</p>', '1663630349_7_aothuntronnutayngan.jpeg', 1, 50, 0, 0, 7, '2022-09-20 06:32:29', '2023-03-16 16:38:50');

-- ----------------------------
-- Table structure for sizes
-- ----------------------------
DROP TABLE IF EXISTS `sizes`;
CREATE TABLE `sizes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sizes
-- ----------------------------
INSERT INTO `sizes` VALUES (1, 'S', '2022-09-13 15:08:46', '2022-09-13 15:08:46');
INSERT INTO `sizes` VALUES (2, 'M', '2022-09-13 15:15:13', '2022-09-13 15:15:13');
INSERT INTO `sizes` VALUES (3, 'L', '2022-09-13 15:15:39', '2022-09-13 15:15:39');
INSERT INTO `sizes` VALUES (6, 'XL', '2022-09-13 16:38:21', '2022-09-13 16:38:43');
INSERT INTO `sizes` VALUES (9, 'XXL', '2022-09-14 07:36:14', '2022-09-14 07:38:32');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
