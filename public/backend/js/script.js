/**
 * Delete Record
 */
function actionDelete(event) {
    event.preventDefault();
    var url = $(this).data('url');
    var that = $(this);
    if (confirm('Bạn muốn xoá bản ghi này ?')) {
        $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function (){
                $.LoadingOverlay('show');
            },
            success: function(data) {
                $.LoadingOverlay('hide');
                alertify.success(data.message);
                that.parent().parent().parent().remove();
            },
            error: function (xhr){
                $.LoadingOverlay('hide');
            }
        })
    }
}
$(function() {
    $(document).on('click', '.deleteItem', actionDelete);
})

/**
 * Change Status with Switch Input
 */
 $('.switch-input').change(function() {
    var status = $(this).prop('checked') == true ? 1 : 0;
    var url = $(this).attr('data-url');
    var id = $(this).attr('data-id');
    
    var success = '<span class="status--process">' +
        'Kích hoạt' +
        '</span>';

    var failed = '<span class="status--denied">' +
        'Không kích hoạt' +
        '</span>';
    $.ajax({
        type:"GET",
        url: url,
        data: {'status': status},
        success: function(response) {
            if (status == 1 ){
                $('#txtstatus'+id).html(success);
                alertify.success(response.message); 
            }else {
                $('#txtstatus'+id).html(failed);
                alertify.success(response.message); 
            }
        },
        error: function() {
            //Lỗi
        }
    })
});

$('.au-checkmark').on('click', function () {
    $(this).parents('.panel').find('.checkbox_childrent').prop('checked', $(this).prop('checked'));
});